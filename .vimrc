execute pathogen#infect()

let mapleader=" "

" line number
set number

syntax enable
set background=dark
" let g:solarized_termcolors = 256
" colorscheme solarized
" let base16colorspace=256
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set background=light " or dark
colorscheme solarized
let g:solarized_termcolors=256
let g:solarized_visibility = "high"
let g:solarized_contrast = "high"
let g:colarized_termtrans = 1
set background=dark
colorscheme solarized

filetype plugin indent on

source ~/.vim/plugin_config.vim

" Disable that damn auto-commenting
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Clipboard and Backspace
set backspace=indent,eol,start

" Underline the current line with '='
nmap <silent> <leader>ul :t.<CR>Vr=

" Search highlighted text on double //
vnoremap // y/<C-R>"<CR>

" Color Theming Stuff
let g:enable_bold_font = 1
"color hybrid_material
"color hybrid_material
hi Normal ctermbg=none
syntax on
